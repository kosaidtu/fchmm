# README #

Converts a fasta-file or multiple fasta-files into a Hidden Markov Model (HMM) made on clustering with 90% sequence identity.

## Quick summary ##
Made for the ResFinder database but can be used for any fasta-files. Clusters the fasta-sequences into clusters with sequence homology of 90%, and creates an HMM for each cluster using a clustal-omega alignment. These HMM's are then merged into one final HMM.
## Version ##
V. 1.0.0

## Summary of set up ##
This program is a python script - just download it to a folder of your choice, and run it like this:

```
#!bash

python fchmm/FChmm_build.py -i FASTA-FILE(S) -o OUTPUT-FOLDER
```
## Configuration ##
No configuration, you may want to symlink the python scripts to your bin-folder.
## Dependencies ##
It comes bundled with binaries for mac and linux. If for some reason, these do not run on your machine, please install the following software:

* clustal-omega/1.2.1
* cd-hit/4.6.1
* hmmer/3.1b2

## How to run ##
Say you have downloaded a database of fasta-files into the folder resfinder_db. Then you run the program as follows:

```
#!bash

python FChmm_build.py -i resfinder_db/*fsa -o HMM
```

Your HMM file will be located in HMM/cluster.hmm

You can now search this HMM database for any given fasta-file like this:

```
#!bash

python FChmm_scan.py --reverse -i myfasta.fsa -o results -hmm HMM/cluster.hmm
```

Your result can now be seen in the file results.best.
If you do not want to have sequences that overlap on the reverse-strand, simply remove the --reverse parameter in the command.


### Who do I talk to? ###
CGE.