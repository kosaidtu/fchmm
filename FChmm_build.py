#!/usr/bin/env python
import argparse
import sys,shutil,subprocess,os,glob

#Importing packages and defining names
pname = "FChmm_build"
pversion = "1.0.0"
git_repo = "https://bitbucket.org/kosaidtu/fchmm"

#Parsing commandline input
parser = argparse.ArgumentParser(prog=pname,usage="python {0}.py -i resfinder_db/*fsa -o amr2hmm".format(pname),description="Version {0}: Converts fasta-files into a single HMM model\n based on 90% sequence homology clustering.".format(pversion," "*(len(pversion)+len("Version : "))),formatter_class=argparse.RawDescriptionHelpFormatter,
  epilog='''
-------------------------
Computerome dependencies:
-------------------------
module load clustal-omega/1.2.1
module load cd-hit/4.6.1
module load hmmer/3.1b2

--------------
How to search:
--------------
When the program has finished running, you can test any fasta
file with the following command:

{0}.py -i FASTA-FILE -o OUTPUT-NAME -hmm amr2hmm/amr.hmm --reverse

See {1} for full documentation.'''.format(pname,git_repo))
parser.add_argument("-i", metavar="FASTA",help="Input fasta-file(s). E.g.: -i my_fasta_files/*fsa", nargs="*",
  default="-")
parser.add_argument("-o", metavar="NAME",help="Output folder.",nargs=1,required=True)
parser.add_argument("-n", metavar="NAME",help="Name of HMM. Default is cluster.hmm",nargs=1,default="cluster.hmm")
parser.add_argument("-t", metavar="INT",help="Number of threads. Default=2",type=int,default=2)
parser.add_argument("--delete",help="Delete temporary files, such as individual HMM's, fasta-files, alignments.",action="store_true")
parser.add_argument("--path",help="Use programs in $PATH instead of included pre-compiled binaries",action="store_true")
args = parser.parse_args() #Get commandline input

#Setting up paths
spath = os.path.dirname(os.path.realpath(sys.argv[0]))
##############################
if sys.platform == "darwin": #if mac
    bpath = spath+"/binaries/darwin/"
else: #assuming linux, not windows
    bpath = spath+"/binaries/linux/"
if args.path: #Use local programs
    bpath = ""

#Creating output folder
try:
 os.mkdir(args.o[0])
 out = args.o[0]+"/"
except Exception as e:
 sys.exit("Error! Cannot create output folder!\n{0}".format(e))
 print(e)

def parsefasta(fid,d): #Saving all fasta entries in a dictionary
 hej = 0
 head = fid.readline().rstrip()
 d[head] = ""
 line = fid.readline().rstrip()
 while line:
  if line[0] == ">":
   head = line
   d[head] = ""
  elif line:
   if ">" in line: #using cat resfinder_db/*fsa > all.fsa gives wrongly formatted fasta file. This corrects it.
    ls = line.split(">")
    d[head] += ls[0]
    head = ">" + ls[1]
    hej += 1
    d[head] = ""
   else:
    d[head] += line
  line = fid.readline().rstrip()
 return d

#Going through ResFinder fasta files
if args.i == "-":
 d = {}
 d = parsefasta(sys.stdin,d) 
else:
 d = {}
 for myfile in args.i:
  with open(myfile) as fid:
   d = parsefasta(fid,d)

#Creating a combined file for fasta-files
with open(out+"all.fa","w") as fout:
 for k,v in d.items():
  fout.write("{0}\n{1}\n".format(k,v))

# make CD-HIT-EST
p = subprocess.check_output('{0}cd-hit-est -i {1}all.fa -o {1}90.fsa -M 10000 -T {2} -d 100 -c 0.90'.format(bpath,out,args.t), stderr=open(out+"programs.log","w"),shell=True)

# Parse cluster file
RF_singles = set()
os.mkdir("{0}/clusters".format(out))
with open("{0}90.fsa.clstr".format(out)) as fid:
        line = fid.readline().rstrip()
        while line:
                if line[0] == ">":
                        cl_num = line.split()[-1]
                        line = fid.readline().rstrip()
                        L = list()
                        while line and line[0] != ">":
                                gene = line.split(", ")[-1].split("...")[0]
                                if "*" in line:
                                        rep = gene
                                L.append(gene)
                                line = fid.readline().rstrip()
                        else:
                                with open("{0}/clusters/RF{1}_{2}".format(out,cl_num,rep[1:]),"w") as fout:
                                        for el in L:
                                                fout.write(el+"\n"+d[el]+"\n")
                                        if len(L) == 1:
                                                RF_singles.add("RF{0}_{1}".format(cl_num,rep[1:]))
                                                fout.write(el+"\n"+d[el]+"\n")

#Aligning and creating a hmm
import pipes
for singleRF in glob.glob("{0}/clusters/RF*".format(out)):
 myrf = singleRF.split("clusters/")[-1]
 single = pipes.quote(singleRF)
 singlehmm = pipes.quote(singleRF+".hmm")
 singleclustal = pipes.quote(singleRF+".clustal")
 if myrf not in RF_singles:
  p = subprocess.check_output('{0}clustalo --threads={3} -i {1} -t DNA -o {2} --outfmt=clustal'.format(bpath,single,singleclustal,args.t), stderr=open(out+"programs.log","w"),shell=True)
  p = subprocess.check_output('{0}hmmbuild --cpu {4} -n {1} {2} {3}'.format(bpath,single,singlehmm,singleclustal,args.t), stderr=open(out+"programs.log","w"),shell=True)
 else:
  p = subprocess.check_output('{0}hmmbuild --cpu {4} -n {1} {2} {3}'.format(bpath,single,singlehmm,single,args.t), stderr=open(out+"programs.log","w"),shell=True)

#Concatenating all hmm into one model, and compiling it
if args.name == "cluster.hmm":
 hmmname = args.n
else:
 hmmname = args.n[0]
with open("{0}/{1}".format(out,hmmname),"w") as destination:
 for single in glob.iglob("{0}/clusters/RF*.hmm".format(out)):
  shutil.copyfileobj(open(single,'rb'), destination)
p = subprocess.check_output('{0}hmmpress {1}/{2}'.format(bpath,out,hmmname), stderr=open(out+"programs.log","w"),shell=True)


#deleting temp files
if args.delete:
 shutil.rmtree("{0}/clusters".format(out))


