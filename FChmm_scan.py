#!/usr/bin/env python
import argparse
import sys,shutil,subprocess,os,glob

#Importing packages and defining names
pname = "FChmm_scan"
pversion = "1.0.0"
git_repo = "https://bitbucket.org/kosaidtu/fchmm"

#Parsing commandline input
parser = argparse.ArgumentParser(prog=pname,usage="python {0}.py -i FASTA-FILE -o OUTPUT -hmm amr.hmm --reverse".format(pname),description="Version {0}: Searches fasta-file against an HMM".format(pversion," "*(len(pversion)+len("Version : "))),formatter_class=argparse.RawDescriptionHelpFormatter,
     epilog='''
-------------------------
Computerome dependencies:
-------------------------
module load hmmer/3.1b2

-----------
How to run:
-----------
If you have an HMM, you run the program like this:

{0}.py -i FASTA-FILE -hmm amr.hmm -o OUTPUT

The results are:
OUTPUT.best: A table with best non-overlapping hits.
OUTPUT.full: A full table of all hits.
OUTPUT.aln: Individual alignments of all hits.

See {1} for full documentation.'''.format(pname,git_repo))
parser.add_argument("-i", metavar="FASTA",help="Input fasta-file(s). E.g.: -i mygenome.fa. REQUIRED.", nargs=1,required=True)
parser.add_argument("-o", metavar="NAME",help="Output base name. Results will be called BASE.full, BASE.best, BASE.aln. REQUIRED.",nargs=1,required=True)
parser.add_argument("-hmm", metavar="HMM",help="Input HMM-file. REQUIRED.",nargs=1,required=True)
parser.add_argument("--reverse",help="Allow overlaps on the reverse strand.",action="store_true")
parser.add_argument("-t", metavar="INT",help="Number of threads. Default=2",type=int,default=2)
parser.add_argument("--path",help="Use programs in $PATH instead of included pre-compiled binaries",action="store_true")
args = parser.parse_args() #Get commandline input


#Setting up paths
spath = os.path.dirname(os.path.realpath(sys.argv[0]))
##############################
if sys.platform == "darwin": #if mac
    bpath = spath+"/binaries/darwin/"
else: #assuming linux, not windows
    bpath = spath+"/binaries/linux/"
if args.path: #Use local programs
    bpath = ""

##Use nhmmscan to search fasta-file against HMM..
base = args.o[0]
infile = args.i[0]
outfile = base+".best"
hmmfile = args.hmm[0]
threads = args.t

p = subprocess.check_output('{0}nhmmscan --cpu {1} -o {2}.aln --tblout {2}.full {3} {4}'.format(bpath,threads,base,hmmfile,infile),shell=True)
#nhmmscan --cpu 4 -o rf.out --tblout rf.tab amr2hmm/amr.hmm FASTA-FILE


## Parse nhmmscan result to remove overlaps
qD = {}
qDn = {}
reverse = args.reverse
#infile = sys.argv[1]
#reverse = True
fulltable = "{0}.full".format(base)
with open(fulltable) as fid:
	myprint = ""
	for line in fid: #Looping over tab file
		if line[0] == "#": #Comment in tab-file
			myprint += line
			continue
		ls = line.split()
		query = ls[2] #query name
		strand = ls[-5] #Strand "+" or "-"
		qstart,qend = sorted( [int(x) for x in ls[6:8]] )
		if query not in qD: #First time we see query
			qD[query] = [(qstart,qend)]
			myprint += line
			continue
		bad = 0
		if query in qD:
			for sstart,send in qD[query]:
				if any( sstart <= x <= send for x in [qstart,qend]   ):
					bad = 1 #Overlap
				elif qstart <= sstart <= qend:
					bad = 1 #Overlap (inclusive)
		if bad == 0 and not reverse: #Rerverse is off, all is good
			myprint += line
			qD[query].append((qstart,qend))
		elif bad == 0 and strand == "+": # Rerverse is on, but no overlap
			myprint += line
			qD[query].append((qstart,qend))
		elif bad == 1 and strand =="-" and reverse:
			if query not in qDn: #First time we see query
				qDn[query] = [(qstart,qend)]
				myprint += line
			else: #We've seen query before
				for sstart,send in qDn[query]:
					if any( sstart <= x <= send for x in [qstart,qend]   ):
						break #Overlap
					elif qstart <= sstart <= qend:
						break #overlap (within)
				else: # Hit on negative strand do not overlap
					myprint += line
					qDn[query].append((qstart,qend))

with open(outfile,"w") as fout: #Printing results
 s = fout.write(myprint)













